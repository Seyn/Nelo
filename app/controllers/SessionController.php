<?php 
class SessionController extends BaseController {

    /**
     * TODO: Validar campos
     */
	
	public function index() {
		

		if( ! Sentry::check()) {
			$user = false;
			return View::make('hello', array('user' => $user));
		} else {
			$user = Sentry::getUser();
			
			return View::make('hello', array('user' => $user));
		}
	}
	
	
	public function getRegister() {
		
		if( Sentry::check() ) {
			return Redirect::to('perfil');
		}

		
		$session_user = Sentry::getUser();
		
		return View::make('pages.register', array('user' => $session_user));
	}
	
    public function do_register() {
		
    	$rules = array(
    			'user' => 'required|min:3|reserved',
    			'name' => 'required|min:3',
    			'email' => 'required|email|unique:web_users',
    			'password' => 'required|min:8',
    			're_password' => 'required|same:password'
    	);
    	
    	Validator::extend('reserved', function($attribute, $value, $parameters)
    	{
    		$reserved_usernames = array('God', 'god','G0D','GOD','Master', 'MASTER', 'master', 'Admin', 'admin', 'ADMIN');
    		if(in_array($value, $reserved_usernames))
    			return false;
    	
    		return true;
    	});
    	
    	$messages = array(
    			'unique' => 'Ya existe una cuenta creada con ese correo electr&oacute;nico.',
    			'email' => 'El email debe ser una direcc&oacute; v&aacute;lida.',
    			'user.min' => 'El usuario debe contener al menos 3 caracteres.',
    			'user.required' => 'El usuario es necesario.',
    			'user.reserved' => 'Este nombre de usuario est&aacute; reservado.'
    	);
    	
    	
    	$validator = Validator::make(Input::all(), $rules, $messages);
    	
    	if($validator->fails()) {
    	
    		$message = $validator->messages();
    	
    		return Redirect::action('SessionController@do_register')
    		->withErrors($validator)
    		->withInput(Input::except('password', 're_password'));
    		 
    	
    	}
    	
    	
    	// Si la validación ha sido correcto se procede a registrar al usuario
    	
    	

    	// GET INFO POST
    	$username = Input::get('user');
    	$name = Input::get('name');
    	$email = Input::get('email');
    	$password = Input::get('password');
    	$send = "";
    	$error = "";
    	
    	
    	try {
    		// Create the user
    		$user = Sentry::createUser(array(
    				'user' 	 	 => $username,
    				'first_name' => $name,
    				'email'      => $email,
    				'password'   => $password,
    				'activated'  => true,
    		));
    	
    		// Find the group using the group id
    		$adminGroup = Sentry::findGroupById(2);
    	
    		// Assign the group to the user
    		$user->addGroup($adminGroup);
	
    	}
    	catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
    	{
    		$error = 'Login field is required.';
    	}
    	catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
    	{
    		$error = 'Password field is required.';
    	}
    	catch (Cartalyst\Sentry\Users\UserExistsException $e)
    	{
    		$error = 'User with this login already exists.';
    	}
    	catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
    	{
    		$error = 'Group was not found.';
    	}
	
    	
    	$account = new Account;
    	$account->name = $username;
    	$account->email = $email;
    	$account->password = $password;
    	$account->save();
    	
    	// redirect
    	Session::flash('message', 'Se ha creado el usuario correctamente');
    	
    	$user = false;
        return View::make('pages/register', array('user' => $user));

    }
    
    
    
    
    public function do_login() {
    	
    	$user = Input::get('user');
    	$password = Input::get('password');
    	
	    try
		{
		    // Login credentials
		    $credentials = array(
		        'email'    => $user,
		        'password' => $password,
		    );
		
		    // Authenticate the user
		    $user = Sentry::authenticate($credentials, true);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    echo 'Wrong password, try again.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User was not found.';
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    echo 'User is not activated.';
		}
		
		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    echo 'User is suspended.';
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    echo 'User is banned.';
		}
    	
    	
    	
    	return Redirect::action('UserController@getProfile');
    }
  	
    public function do_logout() {
    	
    	Sentry::logout();
    	
    	return Redirect::action('SessionController@do_register');
    	
    }
    
}
?>