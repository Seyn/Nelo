<?php

/*
 |--------------------------------------------------------------------------
 | For Error Routes go to start/global.php 
 |--------------------------------------------------------------------------
 |
 | Here is where you can register all of the routes for an application.
 | It's a breeze. Simply tell Laravel the URIs it should respond to
 | and give it the Closure to execute when that URI is requested.
 |
 */

// ------------------------------------

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


// Rutas protegidas

// Todas las peticiones dentro de Route::group deber�n pasar el filtro 'auth'
// Definido en filters.php en el que se comprueba que exista una session (Sentry::check())
Route::group(array('before' => 'auth'), function(){
	Route::get('user', 'UserController@view');
	Route::get('perfil','UserController@getProfile');
	Route::post('edit_email','UserController@edit_email');
	Route::post('edit_password','UserController@edit_password');
});

 

//Rutas publicas

/*
|--------------------------------------------------------------------------
| GET
|--------------------------------------------------------------------------
|
*/

/*
 | Home
*/	
Route::get('/', 
	array(
		'uses' => 'SessionController@index')
);

/*
 | Registro / Login
 */
Route::get('registro', 
	array(
		'uses' => 'SessionController@getRegister')
);

/*
 | Logout
 */
Route::get('logout',
	array(
		'uses' => 'SessionController@do_logout')
);


/*
|--------------------------------------------------------------------------
| POST
|--------------------------------------------------------------------------
|
*/

/*
 | Registro 
 */
Route::post('registro',
	array(
		'uses' => 'SessionController@do_register',
	)
);

/*
 | Login
 */
Route::post('do_login',
	array(
		'uses' => 'SessionController@do_login')
);

/*
 | Actualizar perfil | Email
 */






