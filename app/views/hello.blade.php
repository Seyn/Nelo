@extends('layout/layout')
	@section('title') 
		Home
	@endsection
	
<!-- Nav -->
	@if($user)
		@extends('layout/nav', array('user' => $user))
	@else
		@extends('layout/nav')
	@endif
<!-- End Nav -->	

	@section('header')
	 	<!-- Main jumbotron for a primary marketing message or call to action -->
	    <div class="jumbotron">
	      <div class="container">
	        <h1>Bienvenido a TWOR</h1>
	        <p>Tibia es uno de los juegos m&aacute;s antiguos y exitosos multijugador masivo de rol online (MMORPG) creados en Europa. En un MMORPG gente de todo el mundo se re&uacute;nen en un parque virtual para explorar &aacute;reas, resolver enigmas complicadas y se comprometen haza&ntilde;as heroicas.</p>
	        <p><a class="btn btn-primary btn-lg" href="#" role="button">Saber m&aacute;s &raquo;</a></p>
	      </div>
	    </div>
	@endsection
    
    @section('content')
      <div class="row">
        <div class="col-md-4">
          <h2>TWOR</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">Saber m&aacute;s &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>DOCUMENTACI&Oacute;N</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">Ver documentaci&oacute;n &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>ACERCA DE</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">Saber m&aacute;s &raquo;</a></p>
        </div>
      </div>

      @endsection

    