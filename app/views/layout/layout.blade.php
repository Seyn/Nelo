<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ HTML::script('assets/js/html5shiv.js') }}"></script>
      <script src="{{ HTML::script('assets/js/respond.min.js') }}"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrap">
    	<div class="nav-full">
    		@yield('nav')
    	</div>
    	<div class="header-full">
    		@yield('header')
    	</div>
      <div class="container">
        @yield('content')
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	
	
	
	
	<!-- TODO: Todo en un script.js -->
	<script>
	$(document).ready(function(){
		$('.profile-tabs a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});
		
	});
	</script>
    </body>
</html>