
@section('nav')
<!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">TWOR</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">Home</a></li>
              <li><a href="#">Acerca de</a></li>
              <li><a href="#">Documentaci&oacute;n</a></li>
              <li><a href="#">Contact</a></li>
              @if($user)
	              <li class="dropdown">
	                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{$user['attributes']['user']}} <span class="caret"></span></a>
	                <ul class="dropdown-menu" role="menu">
	                  <li>{{ HTML::link('perfil', 'Perfil', array('class' => 'perfil')) }}</li>
	                  <li><a href="#">Crear personaje</a></li>
	                  <li><a href="#">Ajustes</a></li>
	                  <!-- <li class="divider"></li>
	                  <li class="dropdown-header">Nav header</li>
	                  <li><a href="#">Separated link</a></li>
	                  <li><a href="#">One more separated link</a></li> -->
	                </ul>
	              </li>
	           @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
			  
			 @if($user)
			 	<li><p style="color:#fff;" class="navbar-text">Bienvenido {{$user['attributes']['user']}} | {{ HTML::link('logout', 'Cerrar sesi&oacute;n', array('class' => '')) }}</p> 
				
			 @else
              <li class="active">{{ HTML::link('registro', 'Registro / Login', array('class' => '')) }}</li>
             @endif
              <!--<li><a href="../navbar-static-top/">Static top</a></li>
              <li><a href="../navbar-fixed-top/">Fixed top</a></li> -->
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
@endsection